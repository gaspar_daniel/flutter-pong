import 'dart:ui';
import 'package:flame/game.dart';
import 'package:flame/position.dart';
import 'package:flame/text_config.dart';
import 'dart:math';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class BoxGame extends Game {
  Size screenSize;
  Paint bgPaint = new Paint();
  double uto1_y;
  double uto2_y;
  double labda_x;
  double labda_y;
  double sebesseg_x;
  double sebesseg_y;
  int pont1;
  int pont2;
  Color bg;
  Random random = new Random();
  double t_time = 0;


  void render(Canvas canvas) {
    //Hatter
    Rect bgRect = Rect.fromLTWH(0, 0, screenSize.width, screenSize.height);
    bgPaint.color = bg;
    canvas.drawRect(bgRect, bgPaint);

    //Uto1
    double uto1X = screenSize.width-20;
    Rect boxRect_1 = Rect.fromLTWH(
        uto1X - 5,
        uto1_y - 30,
        10,
        60
    );
    bgPaint.color = Color(0xffffffff);
    canvas.drawRect(boxRect_1, bgPaint);

    //Uto2
    Rect boxRect_2 = Rect.fromLTWH(
        20,
        uto2_y - 30,
        10,
        60
    );
    bgPaint.color = Color(0xffffffff);
    canvas.drawRect(boxRect_2, bgPaint);

    //Labda
    bgPaint.color = Color(0xffffffff);
    canvas.drawCircle(Offset(labda_x, labda_y), 10, bgPaint);

    //Eredményjelző
    String text = pont1.toString()+':'+pont2.toString();
    const TextConfig config = TextConfig(fontSize: 48.0, color: Color(0xffffffff) ,fontFamily: 'Awesome Font');
    config.render(canvas, text, Position(screenSize.width/2-30, 50));
  }

  void update(double t) {
    // Fizika

    //szelen a labda
    if (labda_x >= screenSize.width-30 || labda_x <= 30) {
      sebesseg_x = sebesseg_x * -1;

      if (labda_x >= screenSize.width-30) {
        //Utkozesek
        if (labda_y > uto1_y-30 && labda_y < uto1_y+30) sebesseg_noveles(); else {pont1++;nullaz_sebesseg();}
      } else {
        //Utkozesek
        if (labda_y > uto2_y-30 && labda_y < uto2_y+30) sebesseg_noveles(); else {pont2++;nullaz_sebesseg();}
      }
      if (pont1>4 || pont2>4) nullaz();
    }
    //fuggoleges visszapattanas
    if (labda_y >= screenSize.height-30 || labda_y <= 30) {
      sebesseg_y = sebesseg_y * -1;
    }

    labda_x=labda_x+sebesseg_x;
    labda_y=labda_y+sebesseg_y;

  }


  void sebesseg_noveles() {
    if ( sebesseg_x.abs() < 7) {
      sebesseg_x = sebesseg_x * 1.5;
      if (sebesseg_x.abs()<5) sebesseg_y = random.nextDouble() * 4; else sebesseg_y = random.nextDouble() * 10;
    }
  }
  void nullaz() {
    nullaz_sebesseg();
    pont1=pont2=0;
  }
  void nullaz_sebesseg() {
    uto2_y = uto1_y = screenSize.height/2;
    labda_x=screenSize.width/2;
    labda_y=screenSize.height/2;
    sebesseg_x=2;
    sebesseg_y=2;
  }
  void resize(Size size) {
    screenSize = size;
    bg = Color(0xff000000);
    uto2_y = uto1_y = screenSize.height/2;
    nullaz();
    super.resize(size);
  }

  void dragUpdate(DragUpdateDetails d)
  {
    if (d.globalPosition.dx <= screenSize.width && d.globalPosition.dx >= screenSize.width-50) {
      uto1_y = d.globalPosition.dy;
    }

    if (d.globalPosition.dx <= 50 && d.globalPosition.dx >= 0) {
      uto2_y = d.globalPosition.dy;
    }
  }

}
