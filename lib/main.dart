import 'package:flame/gestures.dart';
import 'package:flame/util.dart';
import 'package:flutter/services.dart';
import 'package:game/box-game.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  Util flameUtil = Util();
  await flameUtil.fullScreen();
  await flameUtil.setOrientation(DeviceOrientation.landscapeLeft);

  BoxGame game = BoxGame();

  //TapGestureRecognizer tapper = TapGestureRecognizer();
  //tapper.onTapDown = game.onTapDown;


  //PanGestureRecognizer if you need both axes
  HorizontalDragGestureRecognizer tapper = HorizontalDragGestureRecognizer();
  tapper.onUpdate = game.dragUpdate;

  runApp(game.widget);
  flameUtil.addGestureRecognizer(tapper);

}
